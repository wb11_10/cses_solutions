#include <iostream>

int main(void) {
    int n;
    long result = 0;
    std::cin >> n;
    int arr[n];

    for (int i = 0; i < n; ++i) std::cin >> arr[i];
    for (int i = 0; i < n - 1; ++i) {
        if (arr[i] > arr[i + 1]) {
            result += arr[i] - arr[i + 1];
            arr[i + 1] = arr[i];
        }
    }
    std::cout << result << std::endl;
    return 0;
}
